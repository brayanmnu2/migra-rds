package com.pragma.entidad;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "usuario")
@Getter
@Setter
public class Usuario {
	
	@Id
	@Column(name = "id")
	private String id;
	
   
    @Column(name = "google_id" , unique = true , nullable = false , length = 50)
    private String googleId;

    @Column(name = "nombres" , length = 50)
    private String nombres;
 
    
    @Column(name = "identificacion" , length = 50)
    private String identificacion;

    @Column(name = "apellidos" , length = 50)
    private String apellidos;
   
    @Column(name = "correo_personal" ,  length = 55)
    private String correoPersonal;

    @Column(name = "correo_empresarial" ,  length = 55 , unique = true)
    private String correoEmpresarial;

    @Column(name = "telefono_fijo" , length = 55)
    private String telefonoFijo;

    @Column(name = "telefono_celular" , length = 55)
    private String telefonoCelular;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;
    
    @Column(name = "direccion")
    private String direccion;

    @Column(name = "nombre_contacto_emergencia" , length = 55)
    private String nombreContactoEmergencia;

    @Column(name = "parentesco_contacto_emergencia" , length = 55)
    private String parentescoContactoEmergencia;

    @Column(name = "telefono_contacto_emergencia" ,  length = 55)
    private String telefonoContactoEmergencia;

    @Column(name = "tarjeta_profesional" , length = 55)
    private String tarjetaProfesional;
    
    @Column(name = "dias_tomados")
    private String diasTomados;
    
    @Column(name = "dias_ganados")
    private String diasGanados;
    
    @Column(name = "perfil_profesional" ,  length = 255)
    private String perfilProfesional;

    @Column(name = "conocimientos_tecnicos")
    private String conocimientosTecnicos;

    @Column(name = "talla_camisa" )
    private String tallaCamisa;

    @Column(name = "estado_civil")
    private String estadoCivil;

    @Column(name = "grupo_sanguineo")
    private String grupoSanguineo;

    @Column(name = "estado")
    private String estado;

    @Column(name = "id_tipo_documento")
    private Long idTipoDocumento;
    
    @Column(name = "id_profesion")
    private Long idProfesion;

    @Column(name = "id_lugar_nacimiento" ) 
    private Long idLugarDeNacimiento;

    @Column(name = "id_lugar_residencia")
    private Long idLugarDeResidencia; 
    
    @Column(name = "id_nacionalidad")
    private Long nacionalidad;

    @Column(name = "fecha_ultima_actualizacion") 
    private LocalDateTime fechaUltimaActualizacion;
    
    @Column(name = "fecha_creacion")
    private LocalDate fechaCreacion; 
    
    @Column(name = "fecha_ingreso")
    private LocalDate fechaIngreso; 
    
    @Column(name = "id_vicepresidencia")
    private Long idVicepresidencia;
    
    //perfiles :parentesco
    
    
}
