package com.pragma.entidad;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "experiencia_laboral_pragma")
@Getter
@Setter
public class ExperienciaLaboralPragma {
	
	@Id
	@Column(name = "id")
	private String id;
	
	@Column(name = "proyecto" , length = 255)
	private String proyecto;
	
	@Column(name = "rol" , length = 255)
	private String rol;
	
	@Column(name = "fecha_de_ingreso")
	private LocalDate fechaDeIngreso;
	
	@Column(name = "fecha_de_finalizacion")
	private LocalDate fechaDeFinalizacion;
	
	@Column(name = "tiempo_de_experiencia")
	private Long tiempoDeExperiencia;
	
	@Column(name = "logros" , length = 655)
	private String logros;
	
	@Column(name = "id_usuario" , length = 255)
	private String idUsuario;

}
