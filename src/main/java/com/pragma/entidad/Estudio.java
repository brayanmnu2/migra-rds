package com.pragma.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "estudio")
@Getter
@Setter
public class Estudio {
	
	@Id
	@Column(name = "id")
	private String id;
	
	private String nivel;
	
	private String idUsuario;
	
	private String tituloObtenido;
	
	private String anioFinalizacion;
	
	private String institucion;

}
