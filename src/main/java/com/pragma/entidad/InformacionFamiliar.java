package com.pragma.entidad;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="informacion_familiar")
@Getter
@Setter
public class InformacionFamiliar {
	

    @Id
    @Column(name = "id", unique = true, nullable = false)
    private String id;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    @Column(name = "genero")
    private String genero;
     
    @Column(name = "id_usuario")
    private String idUsuario;
    
    @Column(name = "id_parentesco")   			
    private Long idParentesco;

}
