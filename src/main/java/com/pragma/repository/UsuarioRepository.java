package com.pragma.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, String> {
	
	Optional <Usuario> findByCorreoEmpresarial(String correoEmpresarial);


}
