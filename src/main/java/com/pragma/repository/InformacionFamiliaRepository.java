package com.pragma.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.entidad.InformacionFamiliar;

public interface InformacionFamiliaRepository extends JpaRepository<InformacionFamiliar , String> {

}
