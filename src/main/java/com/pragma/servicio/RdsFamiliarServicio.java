package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.InformacionFamiliar;
import com.pragma.repository.InformacionFamiliaRepository;

@Service
public class RdsFamiliarServicio {
	
	@Autowired
	InformacionFamiliaRepository informacionFamiliarRepository;

	private static final Logger log = LoggerFactory.getLogger(RdsFamiliarServicio.class);

	int cuantos = 1;

	public void migrarInformacionFamiliar() {

		List<InformacionFamiliar> listicaMelaFamiliar = new ArrayList<>();
		String ruta = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION FAMILIAR\\se-sube-family.json";

		try {
			JSONParser parser = new JSONParser();

			JSONArray listFamiliar = (JSONArray) parser.parse(new FileReader(ruta));

			for (Object familia : listFamiliar) {
				InformacionFamiliar infoFamilia = new InformacionFamiliar();
				JSONObject family = (JSONObject) familia;

				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS :"
								+ cuantos);

				String fechaNacimiento = (String) family.get("fecha_nacimiento");
				LocalDate fechita = null;

				if (!fechaNacimiento.isBlank()) {
					DateFormat formatter = null;
					if (fechaNacimiento.contains("-")) {
						formatter = new SimpleDateFormat("yyyy-MM-dd");
					} else {
						formatter = new SimpleDateFormat("yyyy/MM/dd");
					}
					// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

					Date date = formatter.parse(fechaNacimiento);

					Timestamp timestamp = new Timestamp(date.getTime());

					fechita = timestamp.toLocalDateTime().toLocalDate();
				}

				infoFamilia.setFechaNacimiento(fechita);

				String genero = (String) family.get("genero");
				infoFamilia.setGenero(genero);
				log.info("GENERO: " + genero);

				String apellidos = (String) family.get("apellidos");
				infoFamilia.setApellidos(apellidos);
				log.info("APELLIDOS: " + apellidos);

				String nombre = (String) family.get("nombres");
				infoFamilia.setNombres(nombre);

				Long parentesco = (Long) family.get("id_parentesco");
				infoFamilia.setIdParentesco(parentesco);

				String idUsuario = (String) family.get("id_usuario");
				infoFamilia.setIdUsuario(idUsuario);
				
				String id = (String) family.get("id");
				infoFamilia.setId(id);


				cuantos++;
				listicaMelaFamiliar.add(infoFamilia);

			}

			informacionFamiliarRepository.saveAll(listicaMelaFamiliar);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
