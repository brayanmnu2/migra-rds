package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.ExperienciaLaboralExterna;
import com.pragma.repository.ExperienciaLaboralExternaRepository;

@Service
public class RdsExternaServicio {

	@Autowired
	ExperienciaLaboralExternaRepository experienciaLaboralExternaRepository;

	private static final Logger log = LoggerFactory.getLogger(RdsExternaServicio.class);

	int cuantos = 1;

	public void migrarInformacionLaboralExterna() {

		String rutaJson = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION LABORAL\\se-sube-ext.json";
		List<ExperienciaLaboralExterna> listicaExterna = new ArrayList<>();

		try {
			JSONParser parser = new JSONParser();

			JSONArray listXpPragma = (JSONArray) parser.parse(new FileReader(rutaJson));

			for (Object o : listXpPragma) {

				ExperienciaLaboralExterna xpPragma = new ExperienciaLaboralExterna();
				JSONObject xp = (JSONObject) o;
				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS : "
								+ cuantos);

				String empresa = (String) xp.get("empresa");
				log.info(empresa);
				xpPragma.setEmpresa(empresa);

				String rol = (String) xp.get("rol");
				log.info(rol);
				xpPragma.setRol(rol);

				String fechaIngreso = (String) xp.get("fecha_de_ingreso");
				LocalDate fechitaIngreso = null;
				if (!fechaIngreso.isBlank()) {
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

					Date date = formatter.parse(fechaIngreso);

					Timestamp timestamp = new Timestamp(date.getTime());

					fechitaIngreso = timestamp.toLocalDateTime().toLocalDate();
				}
				xpPragma.setFechaDeIngreso(fechitaIngreso);

				String fechaFinalizacion = (String) xp.get("fecha_de_finalizacion");
				LocalDate fechitaFinal = null;
				if (!fechaFinalizacion.isBlank()) {
					DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

					Date date = formatter.parse(fechaFinalizacion);

					Timestamp timestamp = new Timestamp(date.getTime());

					fechitaFinal = timestamp.toLocalDateTime().toLocalDate();
				}

				xpPragma.setFechaDeFinalizacion(fechitaFinal);

				Long tiempoDeXp = (Long) xp.get("tiempo_de_experiencia");
				xpPragma.setTiempoDeExperiencia(tiempoDeXp);

				String logros = (String) xp.get("logros");
				log.info(logros);
				xpPragma.setLogros(logros);

				String idUsuario = (String) xp.get("id_usuario");
				xpPragma.setIdUsuario(idUsuario);
				
				String id = (String) xp.get("id");
				xpPragma.setId(id);

				cuantos++;
				listicaExterna.add(xpPragma);

			}
			experienciaLaboralExternaRepository.saveAll(listicaExterna);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
