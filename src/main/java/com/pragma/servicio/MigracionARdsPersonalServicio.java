package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.Usuario;
import com.pragma.repository.UsuarioRepository;

@Service
public class MigracionARdsPersonalServicio {

	@Autowired
	UsuarioRepository datosPersonalesRepository;
	
	
	int cuantos = 1;

	public void migrarPragmatico() {

		List<Usuario> usuarito = new ArrayList<Usuario>();
		try {
			JSONParser parser = new JSONParser();

			JSONArray listPragmaticos = (JSONArray) parser.parse(new FileReader("C:\\Users\\Jhonatan.Acosta\\Desktop\\Migraciones\\ultimoS.json"));

			for (Object o : listPragmaticos) {
				Usuario datosPersonales = new Usuario();
				JSONObject person = (JSONObject) o;
				
				System.out.println("--------------CUANTOS: " + cuantos);
				
				/*
				String idTabla = (String) person.get("id");
				datosPersonales.setId(idTabla);*/
  
				String correoEmpresarial = (String) person.get("correo_empresarial");
				Optional<Usuario> usuarioOpt = datosPersonalesRepository.findByCorreoEmpresarial(correoEmpresarial);
				if(usuarioOpt.isPresent()) {
					
					datosPersonales.setCorreoEmpresarial(usuarioOpt.get().getCorreoEmpresarial());
						
					datosPersonales.setId(usuarioOpt.get().getId());
					
					String idUsuario = (String) person.get("google_id");
					datosPersonales.setGoogleId(idUsuario);
					System.out.println("idUsuario = " + idUsuario);

					String nombres = (String) person.get("nombres");
					datosPersonales.setNombres(nombres);
					System.out.println("nombres = " + nombres);

					String numeroIdentificacion = (String) person.get("identificacion");
					datosPersonales.setIdentificacion(numeroIdentificacion);

					String apellidos = (String) person.get("apellidos");
					datosPersonales.setApellidos(apellidos);

					String correoPersonal = (String) person.get("correo_personal");
					datosPersonales.setCorreoPersonal(correoPersonal);
	   
					/*
					String correoEmpresarial = (String) person.get("correo_empresarial");
					datosPersonales.setCorreoEmpresarial(correoEmpresarial);
					*/

					String telefonoFijo = (String) person.get("telefono_fijo");
					datosPersonales.setTelefonoFijo(telefonoFijo);

					String telefonoCelular = (String) person.get("telefono_celular");
					datosPersonales.setTelefonoCelular(telefonoCelular);

					String fechaNacimiento = (String) person.get("fecha_nacimiento");
					LocalDate fechita = null;

					if (fechaNacimiento == null || fechaNacimiento.isBlank()) {
						datosPersonales.setFechaNacimiento(null);
					}

					if (fechaNacimiento != null && !fechaNacimiento.isBlank()) {
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

						Date date = formatter.parse(fechaNacimiento);

						Timestamp timestamp = new Timestamp(date.getTime());

						fechita = timestamp.toLocalDateTime().toLocalDate();
					}

					datosPersonales.setFechaNacimiento(fechita);

					String direccion = (String) person.get("direccion");
					datosPersonales.setDireccion(direccion);

					String nombreContactoDeEmergencia = (String) person.get("nombre_contacto_emergencia");
					datosPersonales.setNombreContactoEmergencia(nombreContactoDeEmergencia);

					String parentescoContactoDeEmergencia = (String) person.get("parentesco_contacto_emergencia");
					datosPersonales.setParentescoContactoEmergencia(parentescoContactoDeEmergencia);

					String telefonoContactoEmergencia = (String) person.get("telefono_contacto_emergencia");
					datosPersonales.setTelefonoContactoEmergencia(telefonoContactoEmergencia);

					String tarjetaProfesional = (String) person.get("tarjeta_profesional");

					if (tarjetaProfesional == null || tarjetaProfesional.equalsIgnoreCase("NULL")) {
						tarjetaProfesional = null;
					}

					datosPersonales.setTarjetaProfesional(tarjetaProfesional);
					System.out.println("Tarjeta " + tarjetaProfesional);

					String perfilProfesional = (String) person.get("perfil_profesional");
					datosPersonales.setPerfilProfesional(perfilProfesional);

					String conocimientosTecnicos = (String) person.get("conocimientos_tecnicos");
					datosPersonales.setConocimientosTecnicos(conocimientosTecnicos);

					String tallaCamisa = (String) person.get("talla_camisa");
					if (tallaCamisa == null) {
						datosPersonales.setTallaCamisa(null);
					}
					datosPersonales.setTallaCamisa(tallaCamisa);

					String estadoCivil = (String) person.get("estado_civil");
					if (estadoCivil == null) {
						datosPersonales.setEstadoCivil(null);
					}
					datosPersonales.setEstadoCivil(estadoCivil);

					String grupo = (String) person.get("grupo_sanguineo");

					datosPersonales.setGrupoSanguineo(grupo);

					String estado = (String) person.get("estado");
					datosPersonales.setEstado(estado);

					Long tipoDocumento = (Long) person.get("id_tipo_documento");
					System.out.println("TIPITO: " + tipoDocumento);
					datosPersonales.setIdTipoDocumento(tipoDocumento);

					Long profesion = (Long) person.get("id_profesion");
					datosPersonales.setIdProfesion(profesion);

					// NO HAY NINGUNA CIUDAD DEL NORTE DE SANTANDER.
					Long lugarDeNacimiento = (Long) person.get("id_lugar_de_nacimiento");
					datosPersonales.setIdLugarDeNacimiento(lugarDeNacimiento);
					datosPersonales.setNacionalidad(lugarDeNacimiento);
					System.out.println(lugarDeNacimiento);

					Long lugarDeResidencia = (Long) person.get("id_lugar_de_residencia");
					datosPersonales.setIdLugarDeResidencia(lugarDeResidencia);
					System.out.println(lugarDeResidencia);

					datosPersonales.setFechaUltimaActualizacion(LocalDateTime.now());

					
					datosPersonales.setDiasGanados(usuarioOpt.get().getDiasGanados());
					datosPersonales.setDiasTomados(usuarioOpt.get().getDiasTomados());
					
					
					datosPersonales.setFechaCreacion(null);

					String fechaIngreso = (String) person.get("fecha_ingreso");

					LocalDate fecha = null;

					if (fechaIngreso == null || fechaIngreso.isBlank()) {
						datosPersonales.setFechaIngreso(null);
					}

					if (fechaIngreso != null && !fechaIngreso.isBlank()) {
						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

						Date date = formatter.parse(fechaIngreso);

						Timestamp timestamp = new Timestamp(date.getTime());

						fecha = timestamp.toLocalDateTime().toLocalDate();
					}

					datosPersonales.setFechaIngreso(fecha);

					Long vice = (Long) person.get("id_vicepresidencia");
					System.out.println("VICE: " + vice);
					datosPersonales.setIdVicepresidencia(vice);

					usuarito.add(datosPersonales);
					cuantos++;
				}
				

			}
			datosPersonalesRepository.saveAll(usuarito);


		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

}
