package com.pragma.servicio;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.entidad.Estudio;
import com.pragma.repository.EstudioRepository;

@Service
public class RdsEstudioServicio {

	@Autowired
	EstudioRepository estudioRepository;
	

	private static final Logger log = LoggerFactory.getLogger(RdsEstudioServicio.class);

	int cuantos = 1;

	public void migrarEstudio() {
		List<Estudio> listicaEstudio = new ArrayList<>();
		String ruta = "C:\\Users\\Jhonatan.Acosta\\Desktop\\RDS-PROD\\INFORMACION ACAD - ESTUDIO\\se-sube-study.json";

		try {
			JSONParser parser = new JSONParser();

			JSONArray listEstudio = (JSONArray) parser.parse(new FileReader(ruta));

			for (Object estudio : listEstudio) {
				Estudio estudios = new Estudio();
				JSONObject study = (JSONObject) estudio;

				System.out.println(
						"//////////////////////////////////////////////////////////////////////-------------------CUANTOS :"
								+ cuantos);

				String nivel = (String) study.get("nivel");
				estudios.setNivel(nivel);
				log.info("NIVEL: " + nivel);

				String idUsuario = (String) study.get("id_usuario");
				estudios.setIdUsuario(idUsuario);

				String tituloObtenido = (String) study.get("titulo_obtenido");
				estudios.setTituloObtenido(tituloObtenido);
				log.info("TITULO OBTENIDO: " + tituloObtenido);

				String anioFinal = (String) study.get("anio_finalizacion");
				estudios.setAnioFinalizacion(anioFinal);
				log.info("AÑO FINALIZACION: " + anioFinal);

				String institucion = (String) study.get("institucion");
				estudios.setInstitucion(institucion);
				log.info("INSTITUCION: " + institucion);
				
				String id = (String) study.get("id");
				estudios.setId(id);

				listicaEstudio.add(estudios);

				cuantos++;

			}
			estudioRepository.saveAll(listicaEstudio);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
