package com.pragma;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.pragma.servicio.MigracionARdsPersonalServicio;
import com.pragma.servicio.RdsEstudioServicio;
import com.pragma.servicio.RdsExternaServicio;
import com.pragma.servicio.RdsFamiliarServicio;
import com.pragma.servicio.RdsPragmaServicio;

@SpringBootApplication
public class MigraRdsApplication implements CommandLineRunner {
	
	@Autowired
	MigracionARdsPersonalServicio migracionARdsPersonalServicio;
	
	@Autowired
	RdsFamiliarServicio rdF;
	
	@Autowired
	RdsEstudioServicio rdE;
	
	@Autowired
	RdsExternaServicio rdX;
	
	@Autowired
	RdsPragmaServicio rdP;

	

	public static void main(String[] args) {
		SpringApplication.run(MigraRdsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//migracionARdsPersonalServicio.migrarPragmatico(); 
		//rdF.migrarInformacionFamiliar(); 
		//rdE.migrarEstudio();
		//rdX.migrarInformacionLaboralExterna();
		//rdP.migrarInformacionLaboralPragma();

	}

}
